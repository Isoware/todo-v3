<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for($i = 0; $i < 10; $i++) {
            $user = new User();
            $user->setEmail("user$i@hotmail.fr");
            $user->setPassword("0000");
            $user->setUsername("User$i");

            if($i == 0) {
                $user->setRoles(['ROLE_ADMIN']);
            }

            $manager->persist($user);
        }

        $user = new User();
        $user->setEmail("unique@hotmail.fr");
        $user->setPassword("0000");
        $user->setUsername("unique");

        $manager->persist($user);

        $manager->flush();
    }
}
