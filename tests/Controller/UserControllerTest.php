<?php

namespace App\Tests\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Liip\TestFixturesBundle\Services\DatabaseToolCollection;
use Liip\TestFixturesBundle\Services\DatabaseTools\AbstractDatabaseTool;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserControllerTest extends WebTestCase
{
    /** @var AbstractDatabaseTool */
    protected $databaseTool;

    public function setUp(): void
    {
        parent::setUp();

        $this->databaseTool = static::getContainer()->get(DatabaseToolCollection::class)->get();
    }

    public function getTestUser(): User
    {
        $userRepository = static::getContainer()->get(UserRepository::class);

        return $userRepository->findOneByEmail('user0@hotmail.fr');
    }

    public function getCreatedUser(): User
    {
        $userRepository = static::getContainer()->get(UserRepository::class);

        return $userRepository->findOneByEmail('UnitTestUser@mail.com');
    }

    public function testList(): void
    {
        self::ensureKernelShutdown();
        $client = static::createClient();

        $this->databaseTool->loadFixtures([
            'App\DataFixtures\UserFixtures',
        ]);

        $client->loginUser($this->getTestUser());

        $client->request('GET', 'admin/users');

        $this->assertResponseIsSuccessful();
    }

    public function testCreate(): void
    {
        self::ensureKernelShutdown();
        $client = static::createClient();
        $client->followRedirects();
        $client->loginUser($this->getTestUser());

        $crawler = $client->request('GET', '/users/create');

        $form = $crawler->selectButton('Ajouter')->form();
        $form['user[username]']->setValue('UnitTestUser');
        $form['user[email]']->setValue('UnitTestUser@mail.com');
        $form['user[password][first]']->setValue('WhatAPass');
        $form['user[password][second]']->setValue('WhatAPass');

        $client->submit($form);

        $this->assertResponseIsSuccessful();
    }

    public function testEdit(): void
    {
        self::ensureKernelShutdown();
        $client = static::createClient();
        $client->followRedirects();
        $client->loginUser($this->getTestUser());

        $crawler = $client->request('GET', '/admin/users/'. $this->getCreatedUser()->getId() .'/edit');

        $form = $crawler->selectButton('Modifier')->form();
        $form['edit_user[username]']->setValue('UnitTestUserEdited');

        $client->submit($form);

        $this->assertEquals('UnitTestUserEdited', $this->getCreatedUser()->getUsername());
        $this->assertResponseIsSuccessful();
    }

    public function tearDown(): void
    {
        parent::tearDown();

        unset($this->databaseTool);
    }
}
