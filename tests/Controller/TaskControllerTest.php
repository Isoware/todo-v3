<?php

namespace App\Tests\Controller;

use App\Entity\Task;
use App\Entity\User;
use App\Repository\TaskRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TaskControllerTest extends WebTestCase
{
    public function getCreatedTask(): Task|null
    {
        $taskRepository = static::getContainer()->get(TaskRepository::class);

        return $taskRepository->findOneByTitle('Test Title');
    }

    public function getTestUser(): User
    {
        $userRepository = static::getContainer()->get(UserRepository::class);

        return $userRepository->findOneByEmail('user0@hotmail.fr');
    }

    public function testList(): void
    {
        $client = static::createClient();

        $client->request('GET', '/tasks');

        $this->assertResponseIsSuccessful();
    }

    public function testCreate(): void
    {
        $client = static::createClient();
        $client->followRedirects();
        $client->loginUser($this->getTestUser());

        $crawler = $client->request('GET', '/tasks/create');

        $form = $crawler->selectButton('Ajouter')->form();
        $form['task[title]']->setValue('Test Title');
        $form['task[content]']->setValue('Test Content');

        $client->submit($form);

        $this->assertResponseIsSuccessful();
    }

    /**
     * @depends testCreate
     */
    public function testEdit(): void
    {
        $client = static::createClient();
        $client->followRedirects();
        $client->loginUser($this->getTestUser());

        $taskTest = $this->getCreatedTask();

        $crawler = $client->request('GET', '/tasks/'. $taskTest->getId() .'/edit');

        $form = $crawler->selectButton('Modifier')->form();
        $form['task[content]']->setValue('Test Content Edited');

        $client->submit($form);

        $this->assertResponseIsSuccessful();
        $this->assertEquals('Test Content Edited', $this->getCreatedTask()->getContent());
    }

    /**
     * @depends testCreate
     */
    public function testToggle(): void
    {
        $client = static::createClient();
        $client->loginUser($this->getTestUser());

        $client->request('GET', '/tasks/'. $this->getCreatedTask()->getId() .'/toggle');

        $this->assertTrue($this->getCreatedTask()->isDone());
    }

    /**
     * @depends testCreate
     */
    public function testDelete(): void
    {
        $client = static::createClient();
        $client->loginUser($this->getTestUser());

        $client->request('GET', '/tasks/'. $this->getCreatedTask()->getId() .'/delete');

        $this->assertNull($this->getCreatedTask());
    }
}
