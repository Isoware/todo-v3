<?php

namespace App\Tests\Entity;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserEntityTest extends WebTestCase
{
    public function assertHasErrors(User $user, int $number)
    {
        $error = static::getContainer()->get('validator')->validate($user);

        $this->assertCount($number, $error);
    }

    public function getValidEntity(): User
    {
        $user = new User();
        $user->setEmail('test@gmail.com');
        $user->setUsername('testUser');
        $user->setPassword('Billip');

        return $user;
    }


    public function testValidEntity()
    {
        $user = $this->getValidEntity();

        $this->assertHasErrors($user, 0);
    }

    public function testInvalidBlankUsername()
    {
        $user = $this->getValidEntity();
        $user->setUsername('');

        $this->assertHasErrors($user, 2);
    }

    public function testInvalidShortUsername()
    {
        $user = $this->getValidEntity();
        $user->setUsername("Abcde");

        $this->assertHasErrors($user, 1);
    }

    public function testInvalidLongUsername()
    {
        $user = $this->getValidEntity();
        $user->setUsername('Abcdefghijqlmnop');

        $this->assertHasErrors($user, 1);
    }

    public function testInvalidBlankMail()
    {
        $user = $this->getValidEntity();
        $user->setEmail('');

        $this->assertHasErrors($user, 1);
    }

    public function testInvalidFormatMail()
    {
        $user = $this->getValidEntity();
        $user->setEmail('test');

        $this->assertHasErrors($user, 1);
    }
}
