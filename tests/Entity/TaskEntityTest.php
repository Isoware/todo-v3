<?php

namespace App\Tests\Entity;

use App\Entity\Task;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class TaskEntityTest extends KernelTestCase
{
    public function assertHasErrors(Task $task, int $number)
    {
        $error = static::getContainer()->get('validator')->validate($task);

        $this->assertCount($number, $error);
    }

    public function getValidEntity(): Task
    {
        $task = new Task();
        $task->setTitle('Test task');
        $task->setContent('Ceci est un test');

        return $task;
    }

    public function testValidEntity()
    {
        $task = $this->getValidEntity();

        $this->assertHasErrors($task, 0);
    }

    public function testInvalidBlankTitle()
    {
        $task = $this->getValidEntity();
        $task->setTitle('');

        $this->assertHasErrors($task, 1);
    }

    public function testInvalidBlankContent()
    {
        $task = $this->getValidEntity();
        $task->setContent('');

        $this->assertHasErrors($task, 1);
    }

    public function testToggleTask()
    {
        $task = $this->getValidEntity();
        $task->toggle(true);

        $this->assertEquals(true, $task->isDone());
    }
}
