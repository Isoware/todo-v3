# API BileMo

## Prérequis
Avoir un hébergeur de base de données (Wamp, Xamp..) et composer

##Installation

- Cloner le projet: git clone https://gitlab.com/Isoware/todo-v3.git
- Lancer composer update
- Lancer yarn install, puis yarn encore dev
- Installer la base de données avec le fichier "todo.sql" (changer la variable "DATABASE_URL" dans le .env)
Lancer le serveur
