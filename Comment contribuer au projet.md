# Comment contribuer au projet

## Forker le projet
Suivre la procédure de gitlab pour créer une copie du projet via Fork.

## Procédure
Créer une merge request indiquant le bug réparé ou le changement effectué. Le nom des commits doivent correspondrent aux changements effectués.

## Tests
Chaque nouvelle fonction doit être testée via phpunit.

## Contraintes
La syntaxe du code doit être respectée, en l'occurence les PSR 1, 4 et 12.
